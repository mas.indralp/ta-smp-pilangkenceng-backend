<?php 
    if(@$_GET['del']){
        $query = $conn->prepare("Delete from t_guru where id='".$_GET['del']."'");
        $query->execute();
    }
    $query = $conn->prepare("SELECT c.nisn, c.nama, SUM(a.poin) as 'poin', (SELECT sanksi FROM t_sanksi WHERE SUM(a.poin) BETWEEN poinmin AND poinmax) AS 'hukuman'
    FROM t_catatan a INNER JOIN t_pelanggaran b ON a.kd_pelanggaran=b.kode INNER JOIN t_siswa c ON a.nipd=c.nipd GROUP BY c.nisn
   ");
    $query->execute();
?>

<div class="row-fluid sortable">
<div class="box span12">
    <div class="box-header" data-original-title>
        <h2><i class="halflings-icon white user"></i><span class="break"></span>Akumulasi Skor Pelanggaran Siswa</h2>
       
    </div>
  
    <div class="box-content">
        <table class="table table-striped table-bordered bootstrap-datatable datatable">
            <thead>
                <tr>
                    <th>NISN</th>
                    <th>Nama Siswa</th>
                    <th>Total Skor</th>               
                    <th>Hukuman</th>               
                    
                </tr>
            </thead>   
            <tbody>
                <?php while($data = $query->fetch()){ ?>
                <tr>	
                   
                    <td><?php echo $data['nisn']; ?></td>
                    <td><?php echo $data['nama']; ?></td>                                                                       
                    <td style="<?php if($data['poin'] >50) { echo "color:red;"; } ?>"> 
                        <?php echo $data['poin']; ?>
                    </td>
                    <td><?php echo $data['hukuman']; ?></td>       
                </tr>
                <?php } ?>
            </tbody>
        </table>            
    </div>
 
</div><!--/span-->
</div>