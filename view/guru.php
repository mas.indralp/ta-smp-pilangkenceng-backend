<?php 
    if(@$_GET['del']){
        $query = $conn->prepare("Delete from t_guru where id='".$_GET['del']."'");
        $query->execute();
    }
    $query = $conn->prepare("Select * from t_guru");
    $query->execute();
?>

<div class="row-fluid sortable">
<div class="box span12">
    <div class="box-header" data-original-title>
        <h2><i class="halflings-icon white user"></i><span class="break"></span>Daftar Guru BK</h2>
       
    </div>
  
    <div class="box-content">
        <table class="table table-striped table-bordered bootstrap-datatable datatable">
            <thead>
                <tr>
                    <th>NIP</th>
                    <th>Nama Guru</th>
                    <th>Jenis Kel</th>
                    <th>No. Telp</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>   
            <tbody>
                <?php while($data = $query->fetch()){ ?>
                <tr>	
                   
                    <td><?php echo $data['nip']; ?></td>
                    <td><?php echo $data['nama']; ?></td>
                    <td><?php if($data['jk']=="L"){echo "Laki-Laki";}else{echo "Perempuan";}; ?></td>
                    <td><?php echo $data['telp']; ?></td>
                    <td><?php echo $data['status']; ?></td>
                
                    <td class="center"> 
                        <a class="btn btn-info" href="index.php?page=EditGuru&id=<?php echo $data['id']; ?>">
                            <i class="halflings-icon white edit"></i>  
                        </a>
                        <a class="btn btn-danger" href="<?php echo $actual_link; ?>&del=<?php echo $data['id']; ?>">
                            <i class="halflings-icon white trash"></i> 
                        </a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>            
    </div>
 
</div><!--/span-->
</div>