<?php 
if($_GET['id']){
    include("./koneksi.php");
    $query = $conn->prepare("Select * from t_identitas where id=".$_GET['id']);
    $query->execute();
    $data=$query->fetch();
}
?>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white edit"></i><span class="break"></span>Form Identitas Sekolah</h2>
            
        </div>
        <div class="box-content">
            <form class="form-horizontal" method="POST" action="save/identitas.php">
                <fieldset>

                <div class="control-group">
                    <label class="control-label">Nama Sekolah</label>
                    <div class="controls">
                        <input class="input-xlarge" name="nama" type="text" placeholder="Nama Sekolah" value="<?php echo @$data['nama']; ?>">
                        <input class="input-xlarge" name="id" type="hidden" value="<?php echo @$data['id']; ?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">NPSN</label>
                    <div class="controls">
                        <input class="input-xlarge"  name="npsn" type="text" placeholder="Isikan NPSN Sekolah" value="<?php echo @$data['npsn']; ?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">NSS</label>
                    <div class="controls">
                        <input class="input-xlarge"  name="nss" type="text" placeholder="NSS Sekolah" value="<?php echo @$data['nss']; ?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Alamat</label>
                    <div class="controls">
                        <input class="input-xlarge"  name="alamat" type="text" placeholder="Alamat Sekolah" value="<?php echo @$data['alamat']; ?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">No. Telp</label>
                    <div class="controls">
                        <input class="input-xlarge"  name="telp" type="text" placeholder="No Telp Sekolah" value="<?php echo @$data['telp']; ?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Kelurahan</label>
                    <div class="controls">
                        <input class="input-xlarge"  name="kelurahan" type="text" placeholder="Kelurahan" value="<?php echo @$data['kelurahan']; ?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Kecamatan</label>
                    <div class="controls">
                        <input class="input-xlarge"  name="kecamatan" type="text" placeholder="Kecamatan" value="<?php echo @$data['kecamatan']; ?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Kota</label>
                    <div class="controls">
                        <input class="input-xlarge"  name="kota" type="text" placeholder="Kota" value="<?php echo @$data['kota']; ?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Provinsi</label>
                    <div class="controls">
                        <input class="input-xlarge"  name="provinsi" type="text" placeholder="Provinsi" value="<?php echo @$data['provinsi']; ?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Website</label>
                    <div class="controls">
                        <input class="input-xlarge"  name="web" type="text" placeholder="Website Sekolah" value="<?php echo @$data['web']; ?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Email</label>
                    <div class="controls">
                        <input class="input-xlarge"  name="email" type="text" placeholder="Email Sekolah" value="<?php echo @$data['email']; ?>">
                    </div>
                </div>

                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                    
                </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->