<?php 
if($_GET['id']){
    include("./koneksi.php");
    $query = $conn->prepare("Select * from t_kurikulum where id=".$_GET['id']);
    $query->execute();
    $data=$query->fetch();
}
?>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white edit"></i><span class="break"></span>Form Kurikulum</h2>
            
        </div>
        <div class="box-content">
            <form class="form-horizontal" method="POST" action="save/kurikulum.php">
                <fieldset>

                <div class="control-group">
                    <label class="control-label">Kurikulum</label>
                    <div class="controls">
                        <input class="input-xlarge" name="kurikulum" type="text" placeholder="Kurikulum" value="<?php echo @$data['kurikulum']; ?>">
                        <input class="input-xlarge" name="id" type="hidden" value="<?php echo @$data['id']; ?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Status</label>
                    <div class="controls">
                      
                        <select name="status" id="status">
                            <option <?php if(@$data['status']=="Aktif"){ echo "selected"; }?> value="Aktif">Aktif</option>
                            <option <?php if(@$data['status']=="Tidak Aktif"){ echo "selected"; }?> value="Tidak Aktif">Tidak Aktif</option>
                        </select>
                    </div>
                </div>            
               
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                    <button type="reset" class="btn">Batal</button>
                </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->