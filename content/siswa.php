<?php 
if($_GET['nipd']){
    $query = $conn->prepare("Select * from t_siswa where nipd=".$_GET['nipd']);
    $query->execute();
    $data=$query->fetch();
}

$query1 = $conn->prepare("Select * from t_kelas");
$query1->execute();


?>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white edit"></i><span class="break"></span>Form Data Siswa</h2>
            
        </div>
        <div class="box-content">
            <form class="form-horizontal" method="POST" action="save/siswa.php">
                <fieldset>

                <div class="control-group">
                    <label class="control-label">NIPD</label>
                    <div class="controls">
                        <input class="input-xlarge" name="nipd" type="text" placeholder="Isikan NIPD Siswa" value="<?php echo @$data['nipd']; ?>">
                        <input class="input-xlarge" name="id" type="hidden" value="<?php echo @$data['id']; ?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">NISN</label>
                    <div class="controls">
                        <input class="input-xlarge"  name="nisn" type="text" placeholder="Isikan NISN Siswa" value="<?php echo @$data['nisn']; ?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Nama Lengkap</label>
                    <div class="controls">
                        <input class="input-xlarge"  name="nama" type="text" placeholder="Nama Lengkap Siswa" value="<?php echo @$data['nama']; ?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Kelas</label>
                    <div class="controls">
                        <select id="kelas" data-rel="chosen" name="kelas" onchange="gantisiswa()">
                            <option value="">-Pilih-</option>
                        <?php while($data1=$query1->fetch()){ ?>    
                            <option value="<?php echo $data1['kelas']; ?>"><?php echo $data1['kelas']; ?></option>
                        <?php } ?>   
                        </select>
                    </div>
                </div>
               
                <div class="control-group">
                    <label class="control-label">Angkatan</label>
                    <div class="controls">
                        <input class="input-xlarge"  name="angkatan" type="text" placeholder="Angkatan Siswa" value="<?php echo @$data['angkatan']; ?>">
                    </div>
                </div>
               
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                    <button type="reset" class="btn">Batal</button>
                </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->