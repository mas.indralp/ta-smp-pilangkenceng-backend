<?php 
 include("./koneksi.php");

if($_GET['id']){
   
    $query = $conn->prepare("Select * from t_pelanggaran where id=".$_GET['id']);
    $query->execute();
    $data=$query->fetch();
}

$qpel = $conn->prepare("Select * from t_pelanggaran");
$qpel->execute();
$dpel=$qpel->rowcount();

?>
<div class="row-fluid sortable">
    <div class="box span12">
    
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white edit"></i><span class="break"></span>Form Data Pelanggaran</h2>
            
        </div>
        <div class="box-content">
            <form class="form-horizontal" method="POST" action="save/pelanggaran.php">
                <fieldset>

                <div class="control-group">
                    <label class="control-label">Kode Pelanggaran</label>
                    <div class="controls">
                        <input class="input-xlarge" name="kode" type="text" placeholder="Kode Pelanggaran" value=" <?php
                            if($_GET['id']){
                                echo @$data['kode']; 
                            }else{
                                echo "C".($dpel+1);
                            }
                        ?>">
                        <input class="input-xlarge" name="id" type="hidden" value="<?php echo @$data['id']; ?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Nama Pelanggaran</label>
                    <div class="controls">
                        <input class="input-xlarge"  name="nama" type="text" placeholder="Isikan Nama Pelanggaran" value="<?php echo @$data['nama']; ?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Poin</label>
                    <div class="controls">
                        <input class="input-xlarge"  name="poin" type="text" placeholder="Poin Pelanggaran" value="<?php echo @$data['poin']; ?>">
                    </div>
                </div>
               
               
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                    <button type="reset" class="btn">Batal</button>
                </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->