<?php 
if($_GET['id']){
    $query = $conn->prepare("Select * from t_catatan where id=".$_GET['id']);
    $query->execute();
    $data=$query->fetch();
 
}

$qkelas = $conn->prepare("Select * from t_kelas");
$qkelas->execute();

$qpel = $conn->prepare("Select * from t_pelanggaran");
$qpel->execute();
?>
<div class="row-fluid sortable">
    <div class="box span12">
    
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white edit"></i><span class="break"></span>Form Data Pelanggaran</h2>
            
        </div>
        <div class="box-content">
            <form class="form-horizontal" method="POST" action="save/catatpel.php">
                <fieldset>

                <div class="control-group">
                    <label class="control-label">Kode Pelanggaran</label>
                    <div class="controls">
                        <select id="pelanggaran" data-rel="chosen" name="kdpel">
                            <option value="">-Pilih-</option>
                        <?php while($dpel = $qpel->fetch()) { ?>
                            <option value="<?php echo $dpel['kode']."-".$dpel['poin']; ?>">
                                <?php echo $dpel['nama']; ?>
                            </option>
                        <?php } ?>  
                        </select>
                        <input class="input-xlarge" id="nilaipoin" name="poin" type="hidden" value="1">
                        <input class="input-xlarge" name="id" type="hidden" value="<?php echo @$data['id']; ?>">
                    </div>
                </div>

                
                <div class="control-group">
                    <label class="control-label">Kelas</label>
                    <div class="controls">
                        <select id="kelas" data-rel="chosen" name="kelas" onchange="gantisiswa()">
                            <option value="">-Pilih-</option>
                        <?php while($dkelas=$qkelas->fetch()){ ?>    
                            <option value="<?php echo $dkelas['kelas']; ?>"><?php echo $dkelas['kelas']; ?></option>
                        <?php } ?>   
                        </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Nama Siswa</label>
                    <div class="controls">
                        <select id="siswapel" data-rel="chosen" name="nipd"></select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Tanggal Pelanggaran</label>
                    <div class="controls">
                    <input type="text" class="input-xlarge datepicker" id="date01" name="tglpel" value="">
                    </div>
                </div>
               
               
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                    <button type="reset" class="btn">Batal</button>
                </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->
