<?php 
if($_GET['id']){
    $query = $conn->prepare("Select * from t_guru where id=".$_GET['id']);
    $query->execute();
    $data=$query->fetch();
}
?>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white edit"></i><span class="break"></span>Form Data Guru BK</h2>
        </div>
        <div class="box-content">
            <form class="form-horizontal" method="POST" action="save/guru.php">
                <fieldset>

                <div class="control-group">
                    <label class="control-label">NIP</label>
                    <div class="controls">
                        <input class="input-xlarge" name="nip" type="text" placeholder="Isikan NIP Guru" value="<?php echo @$data['nip']; ?>">
                        <input class="input-xlarge" name="id" type="hidden" value="<?php echo @$data['id']; ?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Nama Guru</label>
                    <div class="controls">
                        <input class="input-xlarge" name="nama" type="text" placeholder="Isikan Nama Lengkap Guru" value="<?php echo @$data['nama']; ?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Jenis Kelamin</label>
                    <div class="controls">
                        <select name="jk" id="jk">
                            <option <?php if(@$data['jk']=="L"){ echo "selected"; }?> value="L">Laki-Laki</option>
                            <option <?php if(@$data['jk']=="P"){ echo "selected"; } ?>  value="P">Perempuan</option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">No. Telp</label>
                    <div class="controls">
                        <input class="input-xlarge" name="telp" type="text" placeholder="No Telp" value="<?php echo @$data['telp']; ?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Status Pegawai</label>
                    <div class="controls">
                        <select name="sts" id="status">
                            <option <?php if(@$data['status']=="pns"){ echo "selected"; }?>  value="pns">PNS</option>
                            <option <?php if(@$data['status']=="honorer"){ echo "selected"; }?>  value="honorer">Honorer</option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Password</label>
                    <div class="controls">
                        <input class="input-xlarge" name="pass" type="password" placeholder="Password" value="<?php echo @$data['pass']; ?>">
                    </div>
                </div>
               
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                    <button type="reset" class="btn">Batal</button>
                </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->