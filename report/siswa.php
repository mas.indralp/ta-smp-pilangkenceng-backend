<?php 

require_once '../vendor/autoload.php';

$mpdf = new \Mpdf\Mpdf();
include("../koneksi.php");

if($_POST['tgla']){

    $tglawal=date('Y-m-d', strtotime($_POST['tgla']));
    $tglakhir=date('Y-m-d', strtotime($_POST['tglb']));

    $query = $conn->prepare("SELECT a.kd_pelanggaran, c.nama as 'Pelanggaran', a.poin, a.nipd, b.nama as 'Siswa', a.tanggal FROM t_catatan a INNER JOIN t_siswa b ON a.nipd=b.nipd INNER JOIN t_pelanggaran c ON a.kd_pelanggaran=c.kode where tanggal>='".$tglawal."' and tanggal<='".$tglakhir."'");
}else{
    $query = $conn->prepare("SELECT a.kd_pelanggaran, c.nama as 'Pelanggaran', a.poin, a.nipd, b.nisn, b.nama as 'Siswa', a.tanggal FROM t_catatan a INNER JOIN t_siswa b
    ON a.nipd=b.nipd INNER JOIN t_pelanggaran c ON a.kd_pelanggaran=c.kode where b.nisn='".$_POST['nisn']."'");
}

//Jika rincian detail per kelas SELECT a.tanggal, a.kd_pelanggaran, b.nama, a.poin, a.nipd, c.nisn, c.nama, c.kelas FROM t_catatan a INNER JOIN t_pelanggaran b ON a.kd_pelanggaran=b.kode INNER JOIN t_siswa c on a.nipd=c.nipd

//Jika total per siswa per kelas SELECT a.nipd, c.nisn, c.nama, c.kelas, SUM(a.poin) AS Total FROM t_catatan a INNER JOIN t_pelanggaran b ON a.kd_pelanggaran=b.kode INNER JOIN t_siswa c on a.nipd=c.nipd GROUP BY a.nipd
$query->execute();


$datanya = "";
ob_start();

?>

<h1>Data Pelanggaran Siswa</h1>
<table style="border-collapse: collapse;" border='1'>
    <thead>
        <tr>
            <th width="12%">Tanggal</th>
            <th width="12%">NIPD</th>
            <th width="50%">Nama Siswa</th>
            <th width="20%">Pelanggaran</th>
            <th width="10%">Poin</th>
            
        </tr>
    </thead>   
    <tbody>
        <?php while($data = $query->fetch()){ ?>
        <tr>	
            
            <td><?php echo $data['tanggal']; ?></td>
            <td><?php echo $data['nipd']; ?></td>
            <td><?php echo $data['Siswa']; ?></td>
            <td><?php echo $data['Pelanggaran']; ?></td>
            <td><?php echo $data['poin']; ?></td>
        </tr>
        <?php } ?>
    </tbody>
</table> 

<?php
$content = ob_get_contents();
$mpdf->WriteHTML($content);
$mpdf->Output();

?>