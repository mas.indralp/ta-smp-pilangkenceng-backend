<div id="sidebar-left" class="span2">
    <div class="nav-collapse sidebar-nav">
        <ul class="nav nav-tabs nav-stacked main-menu">
            <li><a href="index.php"><i class="icon-bar-chart"></i><span class="hidden-tablet"> Dashboard</span></a></li>	
            <li><a href="<?php echo "index.php?page=CatatPelanggaran"; ?>"><i class="icon-check"></i><span class="hidden-tablet"> Catat Pelanggaran</span></a></li>	
            <li>
                <a class="dropmenu" href="#"><i class="icon-align-justify"></i><span class="hidden-tablet"> Data Master</span></a>
                <ul>
                    <li><a class="submenu" href="<?php echo "index.php?page=Identitas&id=1"; ?>"><i class="icon-info-sign"></i><span class="hidden-tablet"> Identitas Sekolah</span></a></li>
                    <li><a class="submenu" href="<?php echo "index.php?page=ListKelas"; ?>"><i class="icon-book"></i><span class="hidden-tablet"> Data Kelas</span></a></li>
                </ul>	
            </li>
            <li>
                <a class="dropmenu" href="#"><i class="icon-align-justify"></i><span class="hidden-tablet"> Data Pengguna</span></a>
                <ul>
                    <li><a class="submenu" href=<?php echo "index.php?page=ListSiswa"; ?>><i class="icon-file-alt"></i><span class="hidden-tablet"> List Data Siswa</span></a></li>
                    <li><a class="submenu" href=<?php echo "index.php?page=Siswa"; ?>><i class="icon-user"></i><span class="hidden-tablet"> Isi Data Siswa</span></a></li>
                    <li><a class="submenu" href=<?php echo "index.php?page=ListGuru"; ?>><i class="icon-file-alt"></i><span class="hidden-tablet"> List Data Guru</span></a></li>
                    <li><a class="submenu" href=<?php echo "index.php?page=Guru"; ?>><i class="icon-group"></i><span class="hidden-tablet"> Isi Data Guru</span></a></li>
                   
                </ul>	
            </li>
           
           
            <li><a href="<?php echo "index.php?page=ListPelanggaran"; ?>"><i class="icon-check"></i><span class="hidden-tablet"> Data Pelanggaran</span></a></li>	
            <li><a href="<?php echo "index.php?page=ListHukuman"; ?>"><i class="icon-warning-sign"></i><span class="hidden-tablet"> Hukuman</span></a></li>	
            <li><a href="<?php echo "index.php?page=Laporan"; ?>"><i class="icon-bookmark"></i><span class="hidden-tablet"> LAPORAN</span></a></li>	
            <li><a href="<?php echo "index.php?page=SkorSiswa"; ?>"><i class="icon-tag"></i><span class="hidden-tablet"> Skor Siswa</span></a></li>	
            <li><a href="<?php echo "index.php?page=Profile&id=1"; ?>"><i class="icon-bullhorn"></i><span class="hidden-tablet"> PROFILE</span></a></li>	
            <li><a href="<?php echo "index.php?page=Bantuan"; ?>"><i class="icon-leaf"></i><span class="hidden-tablet"> Bantuan</span></a></li>	

        </ul>
    </div>
</div>