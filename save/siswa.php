<!-- Simpan, Edit, Hapus -->
<?php
include("../koneksi.php");
if($_POST['id']){
    //Update data 
     
     $sql=$conn->prepare("Update t_siswa set nipd=:nipd,nisn=:nisn,nama=:nama,kelas=:kls,angkatan=:angkatan where id=:id");
     $data=array(
         ':id'=>$_POST['id'],
         ':nipd'=>$_POST['nipd'],
         ':nisn'=>$_POST['nisn'],
         ':nama'=>$_POST['nama'],
         ':kls'=>$_POST['kelas'],
         ':angkatan'=>$_POST['angkatan'],
     );
     $sql->execute($data);
}else{
    //Simpan data baru
    $sql=$conn->prepare("Insert into t_siswa (nipd,nisn,nama,kelas,angkatan) values(:nipd,:nisn,:nama,:kls,:angkatan)");
    $data=array(
        ':nipd'=>$_POST['nipd'],
        ':nisn'=>$_POST['nisn'],
        ':nama'=>$_POST['nama'],
        ':kls'=>$_POST['kelas'],
        ':angkatan'=>$_POST['angkatan'],
    );

    $sql->execute($data);
   
}
header("Location: http://localhost/sekolah/index.php?page=ListSiswa");
exit;

?>